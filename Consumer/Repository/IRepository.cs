﻿using System.Threading.Tasks;
using Consumer.Model;

namespace Consumer.Repository
{
    public interface IRepository
    { 
        public Task<Order> FindByIdAsync(string id);
        public Task<string> CreateOrderAsync(Order order);
        public Task UpdateOrderAsync(Order order);
        public Task<long> DeleteOrderAsync(string id);
    }
}