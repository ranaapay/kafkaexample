using Confluent.Kafka;

namespace Consumer.Consumers
{
    public interface IKafkaConsumer
    {
        //void Consume(string topic);
        void Subscribe();
        void Consume();
    }
}