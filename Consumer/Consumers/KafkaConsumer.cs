using System;
using System.Collections.Generic;
using System.Reflection.PortableExecutable;
using System.Text.Json.Serialization;
using Confluent.Kafka;
using Consumer.Config;
using Consumer.Model;
using Consumer.Repository;
using Newtonsoft.Json;

namespace Consumer.Consumers
{
    public class KafkaConsumer :IKafkaConsumer
    {
        private readonly ConsumerConfig _config;
        private readonly IConsumer<string, string> _consumer;
        private readonly IRepository _repository;
        public KafkaConsumer(ConsumerConfig config, IRepository repository)
        {
            _repository = repository;
            _config = config;
            _consumer = new ConsumerBuilder<string, string>(_config).Build();
        }
        
        public void Subscribe()
        {
            var topics = new List<string>()
            {
                TopicDeclare.Create,
                TopicDeclare.Update
            };
            _consumer.Subscribe(topics);
        }
        
        public void Consume()
        {
            var consumeResult = _consumer.Consume();
            var order = JsonConvert.DeserializeObject<Order>(consumeResult.Message.Value);

            if (consumeResult.Topic == TopicDeclare.Create)
            {
                _repository.CreateOrderAsync(order);
            }

            if (consumeResult.Topic == TopicDeclare.Update)
            {
                _repository.UpdateOrderAsync(order);
            }
            
            Console.WriteLine($"Consumed event from topic {consumeResult.Topic}, partition : {consumeResult.Partition} " +
                              $"with key : {consumeResult.Message.Key}, value : {consumeResult.Message.Value}, offset value : {consumeResult.Offset}");
        }
    }
}