using CoreLib.Model;

namespace Consumer.Model
{
    public class Order : Document
    {
        public string Name {get; set; }
        public string Email { get; set; }
    }
}