namespace Consumer.Config
{
    public class ConsumerDatabaseSettings
    {
        public string ConnectionString { get; set; }
        public string DatabaseName { get; set; }
    }
}