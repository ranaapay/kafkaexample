namespace Consumer.Config
{
    public class TopicDeclare
    {
        public static string Create { get; set; }
        public static string Update { get; set; }
    }
}