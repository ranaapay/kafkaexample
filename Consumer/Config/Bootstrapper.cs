using Consumer.Consumers;
using Consumer.Repository;
using CoreLib.Mongo.Context;
using Microsoft.Extensions.DependencyInjection;
using MongoDB.Driver;

namespace Consumer.Config
{
    public static class Bootstrapper
    {
        public static void RegisterComponent(IServiceCollection services)
        {
            var settings = services.BuildServiceProvider().GetService<ConsumerDatabaseSettings>();
            RegisterRepositories(services, settings);
        }
        private static void RegisterRepositories(IServiceCollection services, ConsumerDatabaseSettings settings)
        {
            var mongoClient = new MongoClient(settings.ConnectionString);
            var context = new Context(mongoClient, settings.DatabaseName);
            services.AddSingleton<IContext, Context>(provider=>context);
            services.AddSingleton<IRepository, Repository.Repository>();
            services.AddSingleton<IKafkaConsumer, KafkaConsumer>();
        }
    }
}