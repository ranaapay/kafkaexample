using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Confluent.Kafka;
using Consumer.Config;
using Consumer.Consumers;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.VisualBasic;

namespace Consumer
{
    public class Worker : BackgroundService
    {
        private readonly ILogger<Worker> _logger;
        private readonly IKafkaConsumer _kafkaConsumer;

        public Worker(ILogger<Worker> logger, IKafkaConsumer kafkaConsumer)
        {
            _logger = logger;
            _kafkaConsumer = kafkaConsumer;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            try
            {
                _kafkaConsumer.Subscribe();

                while (true)
                {
                    _kafkaConsumer.Consume();
                }
            }
            catch
            {
                Console.WriteLine("Error occured.");
            }
        }
    }
}