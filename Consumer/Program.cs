using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Confluent.Kafka;
using Consumer.Config;
using Consumer.Consumers;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Consumer
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureServices((hostContext, services) =>
                {
                    services.AddHostedService<Worker>();
                    IConfiguration configuration = hostContext.Configuration;
                    
                    var consumerConfig = new ConsumerConfig();
                    configuration.Bind("Consumer",consumerConfig);

                    services.AddSingleton<ConsumerConfig>(consumerConfig);
                    
                    var topicDeclare = configuration.GetSection("TopicDeclare").Get<TopicDeclare>();
                    services.AddSingleton(topicDeclare);
                    
                    var databaseSettings = configuration.GetSection("ConsumerDatabaseSettings").Get<ConsumerDatabaseSettings>();
                    services.AddSingleton(databaseSettings);
                    
                    Bootstrapper.RegisterComponent(services);
                });
    }
}