using Confluent.Kafka;
using CoreLib.Mongo.Context;
using Microsoft.Extensions.DependencyInjection;
using MongoDB.Driver;
using Producer.Repository;
using Producer.Services;

namespace Producer.Config
{
    public static class Bootstrapper
    {
        public static void RegisterComponent(IServiceCollection services)
        {
            var settings = services.BuildServiceProvider().GetService<ProducerDatabaseSettings>();
            RegisterRepositories(services, settings);
        }
        private static void RegisterRepositories(IServiceCollection services, ProducerDatabaseSettings settings)
        {
            var mongoClient = new MongoClient(settings.ConnectionString);
            var context = new Context(mongoClient, settings.DatabaseName);
            services.AddSingleton<IContext, Context>(provider=>context);
            services.AddSingleton<IRepository, Repository.Repository>();
            services.AddSingleton<IProducerService, ProducerService>();
        }
    }
}