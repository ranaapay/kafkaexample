namespace Producer.Config
{
    public class Destination
    {
        public static string Mongo { get; set; }
        public static string Consumer { get; set; }
    }
}