﻿using System;
using System.Reflection.PortableExecutable;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using Confluent.Kafka;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Producer.Config;
using Producer.Model;
using Producer.Model.RequestModels;
using Producer.Services;

namespace Producer.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ProducerController : ControllerBase
    {
        private readonly IProducerService _producerService;
        public ProducerController(IProducerService producerService)
        {
            _producerService = producerService;
        }

        [HttpPost]
        public async Task<IActionResult> CreateEmployee([FromBody] CreateRequestModel createRequestModel)
        {
            var order = createRequestModel.CreateOrderModel();
            await _producerService.HandleCreateOrderRequest(Destination.Consumer, order);
            return Ok(true);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateOrder([FromRoute] string id, [FromBody] UpdateRequestModel updateRequestModel)
        {
            var order = updateRequestModel.OrderForUpdate(id);
            await _producerService.HandleUpdateOrderRequest(Destination.Consumer, order);
            return Ok(true);
        }
        
    }
}