using System;
using System.Threading.Tasks;
using Confluent.Kafka;
using Newtonsoft.Json;
using Producer.Config;
using Producer.Model;
using Producer.Repository;

namespace Producer.Services
{
    public class ProducerService : IProducerService
    {
        private readonly ProducerConfig _config;
        private readonly IProducer<string, string> _producer;
        private readonly IRepository _repository;
        
        public ProducerService(ProducerConfig config, IRepository repository)
        {
            _repository = repository;
            _config = config;
            _producer = new ProducerBuilder<string, string>(_config).Build();
        }
        
        public async Task HandleCreateOrderRequest(string destination, Order order)
        {
            if ( destination == Destination.Mongo )
            { 
                await _repository.CreateOrderAsync(order);
            }

            if ( destination == Destination.Consumer )
            {
                WriteKafka(TopicDeclare.Create,order);
            }
        }
        
        public async Task HandleUpdateOrderRequest(string destination, Order order)
        {
            if ( destination == Destination.Mongo )
            { 
                await _repository.UpdateOrderAsync(order);
            }

            if ( destination == Destination.Consumer )
            {
                WriteKafka(TopicDeclare.Update,order);
            }
        }

        private void WriteKafka(string topic, Order order)
        {
            string serializedOrder = JsonConvert.SerializeObject(order);

            _producer.Produce(topic, new Message<string, string> {Value = serializedOrder},
                (deliveryReport) =>
                {
                    if (deliveryReport.Error.Code != ErrorCode.NoError)
                    {
                        Console.WriteLine($"Failed to deliver message: {deliveryReport.Error.Reason}");
                    }
                    else
                    {
                        Console.WriteLine(
                            $"Produced event to topic {deliveryReport.Topic}: value = {serializedOrder} offset = {deliveryReport.Offset} partition = {deliveryReport.Partition}");
                    }
                });
            _producer.Flush(TimeSpan.FromSeconds(10));
            Console.WriteLine($"Message were produced to topic {topic}");   
        }
    }
}