using System.Threading.Tasks;
using Producer.Model;

namespace Producer.Services
{
    public interface IProducerService
    {
        Task HandleCreateOrderRequest(string destination, Order order);
        Task HandleUpdateOrderRequest(string destination, Order order);
    }
}