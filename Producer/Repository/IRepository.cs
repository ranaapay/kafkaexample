﻿using System.Threading.Tasks;
using Producer.Model;

namespace Producer.Repository
{
    public interface IRepository
    { 
        public Task<Order> FindByIdAsync(string id);
        public Task<string> CreateOrderAsync(Order order);
        public Task UpdateOrderAsync(Order order);
        public Task<long> DeleteOrderAsync(string id);
    }
}