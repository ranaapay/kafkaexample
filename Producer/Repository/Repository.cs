﻿using System;
using System.Threading.Tasks;
using CoreLib.Mongo;
using CoreLib.Mongo.Context;
using MongoDB.Driver;
using Producer.Model;

namespace Producer.Repository
{
    public class Repository : MongoRepository<Order>, IRepository
    {
        public Repository(IContext context, string collectionName="Orders") : base(context, collectionName)
        {
        }

        #region FIND ORDER
        
        public async Task<Order> FindByIdAsync(string id)
        {
            return await FindOneAsync(o => o.Id == id);
        }

        #endregion

        #region CREATE ORDER

        public async Task<string> CreateOrderAsync(Order order)
        {
            return await CreateRecordAsync(order);
        }

        #endregion

        #region UPDATE ORDER

        public async Task UpdateOrderAsync(Order order)
        {
            var update = Builders<Order>.Update
                .Set(o=>o.Name, order.Name)
                .Set(o=>o.Email, order.Email)
                .Set(o=>o.UpdatedAt, DateTime.Now);
            await UpdateRecordAsync(o => o.Id == order.Id, update);
        }
        
        #endregion

        #region DELETE ORDER

        public async Task<long> DeleteOrderAsync(string id)
        {
            return await DeleteOneAsync(o => o.Id == id);
        }

        #endregion
    }
}