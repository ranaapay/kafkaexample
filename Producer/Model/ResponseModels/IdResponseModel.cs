﻿namespace Producer.Model.ResponseModels
{
    public class IdResponseModel
    {
        public string Id { get; set; }
        public string Received { get; set; }

        public IdResponseModel(string id, string received)
        {
            Id = id;
            Received = received;
        }
    }
}