namespace Producer.Model.RequestModels
{
    public class CreateRequestModel
    {
        public string Name { get; set; }
        public string Email { get; set; }
        
        public Order CreateOrderModel()
        {
            return new Order()
            {
                Name = Name,
                Email = Email
            };
        }
    }
}