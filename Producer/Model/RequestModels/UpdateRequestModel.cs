namespace Producer.Model.RequestModels
{
    public class UpdateRequestModel
    {
        public string Name { get; set; }
        public string Email { get; set; }
      
        public Order OrderForUpdate(string id)
        {
            return new Order()
            {
                Id = id,
                Name = Name,
                Email = Email,
            };
        }
    }
}